# βίδα toolbox

This is the high-level repo for finding all other βίδα repos. See the top-level list [here](repos.md).

At present it doesn't show many because previously internally-hosted repos are slowly being made public and will be added there when each is ready.

The βίδα website is still under (re-)construction so for now the best place to find information about βίδα is [here](https://www.facebook.com/veedha.greece/) (yes, that is less than ideal because Facebook is a walled garden, but it is a temporary workaround).

βίδα has been a legally registered non-profit organisation in Greece since 2006. It initially primarily facilitated and coordinated performance and intermedial art. Since then it has increasingly focused on also providing software for those activities, and even for more general support of groups in and around those fields. It remains strictly non-profit and its operational model is based on only covering fees and expenses while providing support to a small selection of projects in a philanthropic manner (providing skills and resources as support, not financial support). All non-site-specific or highly event-specific software developed is then also provided here as F/OSS software for use by others to maximise the chances of extending its usefulness.

Although this repo is just an index for other repos so is essentially contentless, purely for the sake of completeness and uniformity "this repo is licensed under GPLv3, see COPYING for details"
